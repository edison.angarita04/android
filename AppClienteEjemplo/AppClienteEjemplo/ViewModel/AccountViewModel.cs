﻿using AppClienteEjemplo.DTO;
using AppClienteEjemplo.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppClienteEjemplo.ViewModel
{
    class AccountViewModel : UserDTO
    {
        AccountService service = new AccountService();
        UserDTO Model;

        public Command ValidarUsuario { get; set; }

        public AccountViewModel()
        {
            ValidarUsuario = new Command(async () => await Validar(), () => !IsBusy);
        }

        private async Task Validar()
        {
            IsBusy = true;
            if (string.IsNullOrEmpty(NumeroIdentificacion.ToString()))
                MessagingCenter.Send(this, "LoginAlert", NumeroIdentificacion);
            else if (string.IsNullOrEmpty(Password))
                MessagingCenter.Send(this, "LoginAlert", Password);
            else
            {
                Model = new UserDTO()
                {
                    NumeroIdentificacion = this.NumeroIdentificacion,
                    Password = this.Password
                };
                var result = service.validarUsuario(Model);
                await Task.Delay(2000);
            }
            IsBusy = false;
        }


    }
}
