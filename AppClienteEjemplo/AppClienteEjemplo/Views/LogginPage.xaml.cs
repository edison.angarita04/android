﻿using AppClienteEjemplo.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppClienteEjemplo
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LogginPage : ContentPage
	{
        private AccountViewModel _context = new AccountViewModel(); 
        public LogginPage ()
		{

			InitializeComponent ();
            MessagingCenter.Subscribe<AccountViewModel, string>
                (this, "LoginAlert", (sender, username) =>
                {
                    DisplayAlert("Inicio de Sesión fallido", "Complete todos los campos", "OK");
                });
            NumeroIdentificacion.Completed += (Object sender, EventArgs e) =>
            {
                Password.Focus();
            };

            Password.Completed += (Object sender, EventArgs e) =>
            {
                _context.ValidarUsuario.Execute(null);
            };
            BindingContext = _context;

            OptsTypesDocs.Items.Add("Cédula de Ciudadania");
            OptsTypesDocs.Items.Add("Cédula de Extragenría");
            OptsTypesDocs.Items.Add("Tarjeta de Identidad");
            OptsTypesDocs.Items.Add("Nit");

            ValidarButton.Clicked += ValidarButton_Clicked;


        }

        private void ValidarButton_Clicked(object sender, EventArgs e)
        {
            _context.ValidarUsuario.Execute(null);
        }

        private void OptsTypesDocs_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

      
    }
}