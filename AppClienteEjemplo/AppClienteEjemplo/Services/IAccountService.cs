﻿using AppClienteEjemplo.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppClienteEjemplo.Services
{
    interface IAccountService
    {
        
        bool validarUsuario(UserDTO model);
    }
}
