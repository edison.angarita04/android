﻿
using AppClienteEjemplo.DTO;
using System;
using System.Collections.Generic;
using System.Text;


namespace AppClienteEjemplo.Services
{
    class AccountService : IAccountService
    {
        private readonly WebApiAuthIdentityCliente.WebApiAuthIdentityCliente _ApiAuth = new WebApiAuthIdentityCliente.WebApiAuthIdentityCliente();

        

        public bool validarUsuario(UserDTO model)
        {
            var result = _ApiAuth.GetAuth(Convert.ToDecimal(model.NumeroIdentificacion), model.Password).Result;
            if (result.Length > 0)
                return true;
            else
                return false;
        }
    }
}
